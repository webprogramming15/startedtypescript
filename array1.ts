const names: string[] = [];
names.push("Dylan");
names.push("Peeraya");
console.log(names[0]);
console.log(names[1]);
console.log(names.length);

console.log("----------")
for (let i=0; i<names.length; i++) {
    console.log(names[i]);
}

console.log("----------")
for (let ind in names) {
    console.log(names[ind]);
}

console.log("----------")
names.forEach(function(name) {
    console.log(name);
});